(($, Drupal, drupalSettings) => {
  /**
   * Handles the Pick Pack Pont selector in checkout.
   *
   * @type {Drupal~behavior}
   *
   * @prop {Drupal~behaviorAttach} attach
   */
  Drupal.behaviors.pickPackPontSelected = {
    attach(context) {
      if (once("hu-pickpackpont", "html").length) {
        $(window).on("message onmessage", function (e) {
          var data = JSON.parse(e.originalEvent.data);
          $("input[name^='pickup_capable_shipping_information[shipping_profile][pickup_dealer][id]']").val(data.shopCode);
          $("input[name^='pickup_capable_shipping_information[shipping_profile][pickup_dealer][name]']").val(data.shopName);
          $("input[name^='pickup_capable_shipping_information[shipping_profile][pickup_dealer][address]']").val(data.address);
        });
      }
    }
  };
})(jQuery, Drupal, drupalSettings);