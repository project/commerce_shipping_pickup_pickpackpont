<?php

namespace Drupal\commerce_shipping_pickup_pickpackpont\Plugin\Commerce\ShippingMethod;

use Drupal\commerce_shipping_pickup_api\Plugin\Commerce\ShippingMethod\PickupShippingMethodBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\profile\Entity\ProfileInterface;

/**
 * Provides a specific pickup shipping method.
 *
 * @CommerceShippingMethod(
 *   id = "pickup_hu_pickpackpont",
 *   label = @Translation("Pickup shipping - Pick Pack Pont"),
 * )
 */
class PickPackPontShipping extends PickupShippingMethodBase {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function buildFormElement(ProfileInterface $profile): array {
    $element = [
      '#type' => 'container',
    ];
    $element['selector'] = [
      '#type' => 'inline_template',
      '#template' => '<iframe width="100%" height="590px" src="{{ url }}"></iframe>',
      '#context' => ['url' => 'https://online.sprinter.hu/terkep/#/'],
    ];
    $element['id'] = [
      '#type' => 'hidden',
      '#required' => TRUE,
    ];
    $element['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#attributes' => ['readonly' => 'readonly'],
      '#required' => TRUE,
    ];
    $element['address'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Address'),
      '#attributes' => ['readonly' => 'readonly'],
      '#required' => TRUE,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function populateProfile(ProfileInterface $profile): void {
    $data = $profile->getData('pickup_location_data');
    $profile->set('address', [
      'country_code' => 'HU',
      'organization' => $data['name'],
      'address_line1' => $data['address'],
    ]);
  }
}
